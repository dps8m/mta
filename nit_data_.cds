/* nit_data_.cds */

nit_data_: proc;

  dcl 1 cdsa aligned like cds_args;
  dcl code  fixed bin (35);
  dcl com_err_  entry options (variable);
  dcl create_data_segment_  entry (pointer, fixed binary (35));



  dcl 1 nit_data aligned,
    2 local_host character (40) varying;

  local_host = "localhost";

  unspec (cdsa) = ""b;
  cdsa.sections (1) /* text */ .p = addr (nit_data);
  cdsa.sections (1).len = currentsize (nit_data);
  cdsa.sections (1).struct_name = "nit_data";

  cdsa.seg_name = "nit_data_";
  cdsa.num_exclude_names = 0;
  cdsa.exclude_array_ptr = null ();
  cdsa.switches.have_text = "1"b;

  call create_data_segment_ (addr (cdsa), code);
  if code ^= 0 then
    call com_err_ (code, "nit_data_");
  return;

%include cds_args;
end;

