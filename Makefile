all : install >udd>Daemon>Network_Server>outgoing_mail >udd>Daemon>Network_Server>bounce

>udd>Daemon>Network_Server:
	create_dir >udd>Daemon>Network_Server
	add_name >udd>Daemon>Network_Server ns
	set_acl >udd>Daemon>Network_Server sma Network_Server.*
	set_iacl_seg >udd>Daemon>Network_Server rw Network_Server.*

>udd>Daemon>Network_Server>bounce: >udd>Daemon>Network_Server
	ms_create >udd>Daemon>Network_Server>bounce
	ms_set_acl >udd>Daemon>Network_Server>bounce adros Network_Server.* aos *.*


>udd>Daemon>Network_Server>outgoing_mail :
	ms_create >udd>Daemon>Network_Server>outgoing_mail
	ms_set_acl >udd>Daemon>Network_Server>outgoing_mail adros Network_Server.* aos *.*


install : nit_ nit_data_ host_table_mgr_ mlsys_mailer_ hostname bound_mta_server
	dl nit.io
	us in nit 
	us add nit_ >sss>== -acl re *
	us add nit_data_ >sss>== -acl rw *
	us add host_table_mgr_ >sss>== -acl re *
	us add mlsys_mailer_ >sss>== -acl re *
	us add hostname >sss>== -acl re *
	us add bound_mta_server >sss>== -acl re *
	us ls
	us pr
	us install

reinstall : nit_ nit_data_ host_table_mgr_ mlsys_mailer_ hostname bound_mta_server
	dl nit.io
	us in nit 
	us rp nit_ >sss>== -acl re *
	us rp nit_data_ >sss>== -acl rw *
	us rp host_table_mgr_ >sss>== -acl re *
	us rp mlsys_mailer_ >sss>== -acl re *
	us rp hostname >sss>== -acl re *
	us rp bound_mta_server >sss>== -acl re *
	us ls
	us pr
	us install

nit_ : nit_.pl1
	pl1 nit_

nit_data_ : nit_data_.cds
	cds nit_data_

host_table_mgr_ : host_table_mgr_.pl1
	pl1 host_table_mgr_

mlsys_mailer_ : mlsys_mailer_.pl1
	pl1 mlsys_mailer_

hostname : hostname.pl1
	pl1 hostname

mta_server_: mta_server_.pl1
	pl1 mta_server_

mta_: mta_.pl1
	pl1 mta_

mta_et_: mta_et_.alm
	alm mta_et_

bound_mta_server : mta_server_ mta_ mta_et_ bound_mta_server.bind
	bind -sm bound_mta_server.bind mta_server_ mta_ mta_et_ -bdf bound_mta_server 

ta:
	dl nit.ta -fc
	ta a nit Makefile host_table_mgr_.pl1 hostname.pl1 mlsys_mailer_.pl1 nit_.pl1 nit_data_.cds mta_server.pl1 mta_.pl1 mta_et_.alm
	ta go nit
